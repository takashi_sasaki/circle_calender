class CircleEvent < ActiveRecord::Base
    validates :circle_id,    presence: true, length:{maximum:11}
    validates :event_date,   presence: true
    validates :event_detail, presence: true, length:{maximum:500}
    validates :user_id,      presence: true, length:{maximum:11}
end
