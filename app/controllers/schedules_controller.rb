class SchedulesController < ApplicationController
  def create
    @circle_event = CircleEvent.new(user_params)
    if @circle_event.save
      render 'show'
    else
      render 'new'
    end
  end

  def edit
  end  
  
  def new
    @circle = Circle.new
    @circle_event = CircleEvent.new
  end  
  
  def show
    @circle_event = CircleEvent.new
  end
  
  private

    def user_params
      params.require(:circle_event).permit(:circle_id, :event_date, :event_detail,:user_id)
    end
end
