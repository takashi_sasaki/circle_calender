class CirclesController < ApplicationController
  def show
    @circle = Circle.find(params[:id])
    if @circle.official_flg
      @official = '公式部活動'
    else
      @official = '非公式部活動'
    end
  end
  
  def new
    @circle = Circle.new
  end
  
  def create
    @circle = Circle.new(circle_params)
    if @circle.save
      redirect_to @circle
    else
      render 'new'
    end
  end
  
  private

  def circle_params
    params.require(:circle).permit(:name, :detail, :official_flg)
  end
end
