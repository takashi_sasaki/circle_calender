# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151119095729) do

  create_table "circle_events", force: :cascade do |t|
    t.integer  "circle_id"
    t.datetime "event_date"
    t.text     "event_detail"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "circle_events", ["circle_id"], name: "index_circle_events_on_circle_id"
  add_index "circle_events", ["event_date"], name: "index_circle_events_on_event_date"

  create_table "circles", force: :cascade do |t|
    t.string   "name"
    t.integer  "official_flg"
    t.integer  "delete_flg"
    t.string   "detail"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "circles", ["official_flg", "delete_flg"], name: "flgs"
  add_index "circles", ["official_flg"], name: "official_flg"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "detail"
    t.string   "remember_digest"
  end

end
