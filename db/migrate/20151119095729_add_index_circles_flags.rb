class AddIndexCirclesFlags < ActiveRecord::Migration
  def change
    add_index "circles", ["official_flg", "delete_flg"], name: "flgs"
    add_index "circles", ["official_flg"], name: "official_flg"
  end
end
