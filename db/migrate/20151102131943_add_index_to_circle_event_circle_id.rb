class AddIndexToCircleEventCircleId < ActiveRecord::Migration
  def change
    add_index :circle_events, :circle_id
  end
end
