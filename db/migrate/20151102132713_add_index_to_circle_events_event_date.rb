class AddIndexToCircleEventsEventDate < ActiveRecord::Migration
  def change
    add_index :circle_events, :event_date
  end
end
