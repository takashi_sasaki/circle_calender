class CreateCircleEvents < ActiveRecord::Migration
  def change
    create_table :circle_events do |t|
      t.integer :circle_id
      t.datetime :event_date
      t.text :event_detail
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
