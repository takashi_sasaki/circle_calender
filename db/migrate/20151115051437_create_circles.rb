class CreateCircles < ActiveRecord::Migration
  def change
    create_table :circles do |t|
      t.string :name
      t.integer :official_flg
      t.integer :delete_flg
      t.string :detail

      t.timestamps null: false
    end
  end
end
