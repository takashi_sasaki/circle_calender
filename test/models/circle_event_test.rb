require 'test_helper'

class CircleEventTest < ActiveSupport::TestCase
  def setup
    @circleevent = CircleEvent.new(circle_id: 1, event_date: "2014-01-02 23:45:00",event_detail:"testTEXT",user_id:1)
  end

  test "should be valid" do
    assert @circleevent.valid?
  end
end
