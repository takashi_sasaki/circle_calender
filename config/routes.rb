Rails.application.routes.draw do
  root 'top#index'
  
  get 'signup'  => 'users#new'
  #get 'create_user'  => 'users#new'
  get 'create_circle'  => 'circles#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  get  'schedules/show'
  get  'schedules/new'
  post 'schedules/new'
  post 'schedules/create'
  get 'schedules/edit'



  resources :users
  resources :circles
  resources :circle_events

end
